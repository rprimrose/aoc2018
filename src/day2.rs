use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn day2() {
    let filename = "./day2input.txt";
    let mut entries = vec![];
    let f = File::open(filename).expect("file not found");
    let buf = BufReader::new(f);
    for line in buf.lines() {
        entries.push(
            line.expect("Unable to read line")
                .parse::<String>()
                .unwrap()
        );
    }
    part1(entries.to_vec());
    part2(entries.to_vec());
}

fn part1(entries: Vec<String>) {
    let mut two_count = 0;
    let mut three_count = 0;
    for entry in entries {
        let mut sorted = sort(entry);
        let mut found_two = false;
        let mut found_three = false;
        while sorted.len() > 0 {
            let current_char = sorted.chars().next().unwrap();
            let last = sorted.rfind(current_char).unwrap();
            if last == 1 {
                found_two = true
            };
            if last == 2 {
                found_three = true
            };
            sorted.drain(..last + 1);
        }
        if found_two {
            two_count += 1
        }
        if found_three {
            three_count += 1
        }
    }
    let checksum = two_count * three_count;
    println!("{}", checksum)
}

fn part2(entries: Vec<String>) {
    let entries2 = entries.to_vec();
    for entry in entries {
        for entry2 in &entries2 {
            let diff = string_difference(&entry, &entry2);
            if diff == 1 {
                println!("{} - {}", entry, entry2);
                return;
            }
        }
    }
}

fn string_difference(a: &String, b: &String) -> i32 {
    let a_chars = a.chars();
    let mut b_chars = b.chars();
    let mut diff = 0;
    for thing in a_chars {
        if !(thing == b_chars.next().unwrap()) {
            diff += 1;
        }
    }
    return diff;
}

fn sort(s: String) -> String {
    let mut temp = s.chars().collect::<Vec<char>>();
    temp.sort();
    temp.into_iter().collect::<String>()
}
