extern crate lazy_static;
extern crate regex;

mod day3;

fn main() {
    day3::day3();
}
