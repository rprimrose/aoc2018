use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn day1() {
    let filename = "./day1input.txt";
    let mut changes = vec![];
    let f = File::open(filename).expect("file not found");
    let buf = BufReader::new(f);
    for line in buf.lines() {
        changes.push(line.expect("Unable to read line").parse::<i32>().unwrap());
    }

    part1(&changes);
    part2(changes);
}

fn part1(changes: &Vec<i32>) {
    println!("total {}", changes.iter().sum::<i32>());
}

fn part2(changes: Vec<i32>) {
    let mut current_frequency = 0;
    let mut frequencies = HashSet::new();
    frequencies.insert(current_frequency);
    loop {
        for change in &changes {
            current_frequency += change;
            if !frequencies.insert(current_frequency) {
                println!("duplicate {}", current_frequency);
                return;
            }
        }
    }
}
