use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use regex::Regex;
use std::collections::HashMap;

pub fn day3() {
    let filename = "./day3input.txt";
    let mut entries: Vec<Claim> = vec![];
    let f = File::open(filename).expect("file not found");
    let buf = BufReader::new(f);
    for line in buf.lines() {
        let mut entry = line.expect("Unable to read line")
            .parse::<String>()
            .unwrap();

        let re = Regex::new(
            r#"#(?P<id>\d*)\s@\s(?P<x>\d*),(?P<y>\d*):\s+(?P<width>\d+)x(?P<height>\d+)"#,
        ).unwrap();
        let caps = re.captures(entry.as_str()).unwrap();
        let _claim = Claim {
            id: caps["id"].parse::<i32>().unwrap(),
            x: caps["x"].parse::<i32>().unwrap(),
            y: caps["y"].parse::<i32>().unwrap(),
            width: caps["width"].parse::<i32>().unwrap(),
            height: caps["height"].parse::<i32>().unwrap(),
        };
        entries.push(_claim);
    }
    part1(entries);
}

fn part1(entries: Vec<Claim>) {
    let mut squares: HashMap<(i32, i32), Vec<i32>> = HashMap::new();
    for entry in entries {
        let end_x = entry.x + entry.width;
        let end_y = entry.y + entry.height;
        for x in entry.x..end_x {
            for y in entry.y..end_y {
                let square: &mut Vec<i32> = squares.entry((x, y)).or_insert(vec![]);
                square.push(entry.id);
            }
        }
    }
//    let duplicates = squares.into_iter()
//        .filter(|&(_, ref v)| v.len() > 1)
//        .count();
//    println!("part1 {}", duplicates);

    let unique: Vec<_> = squares.into_iter()
        .filter(|&(_, ref v)| v.len() == 1)
        .map(|(_, v)| v)
        .collect::<Vec<_>>();
    for un in unique {
        println!("part 2 {:?}", un);
    }

}

#[derive(Debug)]
struct Claim {
    id: i32,
    x: i32,
    y: i32,
    width: i32,
    height: i32,
}